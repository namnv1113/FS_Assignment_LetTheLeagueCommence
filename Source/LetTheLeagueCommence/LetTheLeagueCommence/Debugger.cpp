// This file contains function implementations for the class "Debugger"

#include "Debugger.h"

vector<Log> Debugger::logs;

void Debugger::AppendLog(string description, int logTypeIdentifier)
{
	// Create new log and push data
	Log* newLog = new Log();
	newLog->information = description;
	newLog->type = logTypeIdentifier;

	// Store into list
	logs.push_back(*newLog);
}

vector<Log> Debugger::GetLogs()
{
	return logs;
}

void Debugger::ClearLogs()
{
	logs.clear();
}
