// A file contains function implementations for "FileManager" class

#include "FileManager.h"
#include <fstream>
#include "Debugger.h"
#include "Constants.h"
#include <sys/stat.h>

string FileManager::folderPath;			// Neccesary for static variable

FileManager::FileManager()
{
	FileManager::folderPath = "";
}

string FileManager::GetFolderPath()
{
	return FileManager::folderPath;
}

bool FileManager::SetFolderPath(string path)
{
	// Replace default path
	folderPath = path;
	if (folderPath[folderPath.size() - 1] != PATH_SPLITTER)
		folderPath.append(string(1, PATH_SPLITTER));
	Debugger::AppendLog(SET_WORKING_DIR_SUCCESS_NOTIFICATION, DEBUGGER_INFOR_LOG_ID);
	return true;
}

vector<string> FileManager::ReadFromFile(string fileName)
{
	// Open file
	fstream file;
	file.open(fileName, ios::in);
	// Check if file is opened
	if (file.fail())
	{
		// If not open then write to log & return null
		Debugger::AppendLog(CANT_OPEN_FILE_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ERROR_FILE_DETAILS_STR + fileName, DEBUGGER_ERROR_LOG_ID);
		return vector<string>();
	}

	// Read data from file, one line at a time
	string temp;
	vector<string> res;
	while (!file.eof())
	{
		getline(file, temp);
		res.push_back(temp);
	}

	// Close file & return results
	file.close();
	return res;
}

bool FileManager::WriteToFile(vector<string> data, string fileName)
{
	// Open file - If exist --> Overwrite
	fstream file;
	file.open(fileName, ios::out);
	// Check if file is opened
	if (file.fail())
	{
		// If file is not opened, write to log and return false
		Debugger::AppendLog(CANT_OPEN_FILE_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ERROR_FILE_DETAILS_STR + fileName, DEBUGGER_ERROR_LOG_ID);
		return false;
	}

	// Write data to file, one line at a time
	for (int i = 0; i < data.size() - 1; i++)
		file << data[i] << endl;
	file << data[data.size() - 1];

	// Close file & return
	file.close();
	return true;
}

bool FileManager::IsExist(string fileName)
{
	struct stat buffer;
	return (stat(fileName.c_str(), &buffer) == 0);
}
