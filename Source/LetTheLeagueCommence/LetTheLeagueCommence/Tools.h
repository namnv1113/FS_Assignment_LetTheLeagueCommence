// This file contains the "FileManager" class defintion where file management are handled
#pragma once
#include <string>
#include <vector>
using namespace std;

class Tools
{
	public:
		static bool CopyFile(string source, string destination);			// Copy a file from source path to destination path
		static vector<string> Split(string data, char splitter);			// Split a string (data) by many multiple strings using a splitter, which is a char
		static string IntToString(int data);								// Convert integer to string
		static string WindowsStringToString(System::String^ data);			// Convert System::string to std::string
		static System::String^ StringToWindowString(string data);			// Convert std::string to System::string
		static string Join(vector<string> data, char joiner);				// Join vector of strings into a single string, split by a character 'joiner'
};
