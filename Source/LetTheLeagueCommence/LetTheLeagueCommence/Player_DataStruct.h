// This file contains data struct that are related to player

#pragma once
#include <string>
#include <vector>
using namespace std;

// Data structs that represent a player update notification/action
struct PlayerUpdate
{
	int action;			// Represent a player update in a club, include add & delete (see constants.h)
	string clubName;	// The club where the player is added or deleted
	string playerName;	// Represent player name
};
