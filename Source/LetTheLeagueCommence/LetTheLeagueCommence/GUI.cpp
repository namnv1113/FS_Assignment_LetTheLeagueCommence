#include "GUI.h"
#include "Debugger.h"
#include "Constants.h"
#include "FileManager.h"
#include "League.h"
#include "Tools.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThread]
void Main()
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	LetTheLeagueCommence::GUI mainMenuForm;
	Application::Run(%mainMenuForm);
}

System::Void LetTheLeagueCommence::GUI::btnBrowse_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	if (folderBrowserDialog_InputFolder->ShowDialog() != System::Windows::Forms::DialogResult::Cancel)		// If a path is chosen from an open dialog
	{
		textBoxFilePath->Text = folderBrowserDialog_InputFolder->SelectedPath;		// Print text to screen

		// Store path for future use
		FileManager::SetFolderPath(Tools::WindowsStringToString(textBoxFilePath->Text));
		isPathSpecified = true;
	}
}

System::Void LetTheLeagueCommence::GUI::buttonRun_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	if (!isPathSpecified)		// If a path is not yet input
	{
		// Print error & quit
		Debugger::AppendLog(SPECIFY_FILE_PATH_NOTIFICATION + INPUT_FILENAME, DEBUGGER_ERROR_LOG_ID);
		printLogs();
		return;
	}

	// Execute commands
	League myLeague;
	myLeague.Execute();

	// Print logs
	printLogs();
}

System::Void LetTheLeagueCommence::GUI::buttonClearAll_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	if (MessageBox::Show(Tools::StringToWindowString(CLEAR_ALL_DATA_WARNING_DESCRIPTION), Tools::StringToWindowString(CLEAR_ALL_DATA_WARNING_TITLE), MessageBoxButtons::YesNo, MessageBoxIcon::Warning) == System::Windows::Forms::DialogResult::Yes)
	{
		// Clear logs
		richTextBox_Logs->ResetText();
		Debugger::ClearLogs();
		//textBoxFilePath->ResetText();
		//FileManager::SetFolderPath("");
		//isPathSpecified = false;
	}
}

System::Void LetTheLeagueCommence::GUI::printLogs()
{
	// Get unprinted logs from debugger
	vector<Log> logs = Debugger::GetLogs();
	for (int i = 0; i < logs.size(); i++)
	{
		richTextBox_Logs->SelectionStart = richTextBox_Logs->TextLength;
		richTextBox_Logs->SelectionLength = 0;

		// Specify color
		if (logs[i].type == DEBUGGER_INFOR_LOG_ID)
			richTextBox_Logs->SelectionColor = Color::Blue;
		else if (logs[i].type == DEBUGGER_ERROR_LOG_ID)
			richTextBox_Logs->SelectionColor = Color::Red;
		else if (logs[i].type == DEBUGGER_WARNI_LOG_ID)
			richTextBox_Logs->SelectionColor = Color::Purple;
		else if (logs[i].type == DEBUGGER_NORMA_LOG_ID)
			richTextBox_Logs->SelectionColor = Color::Black;

		richTextBox_Logs->AppendText(Tools::StringToWindowString(logs[i].information.c_str()) + Environment::NewLine);
		richTextBox_Logs->SelectionColor = richTextBox_Logs->ForeColor;
	}
	Debugger::ClearLogs();
}
