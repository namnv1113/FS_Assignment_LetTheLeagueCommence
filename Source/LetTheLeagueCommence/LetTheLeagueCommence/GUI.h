#pragma once

namespace LetTheLeagueCommence {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for GUI
	/// </summary>
	public ref class GUI : public System::Windows::Forms::Form
	{
	public:
		GUI(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~GUI()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Button^  buttonBrowse;
	private: System::Windows::Forms::Label^  labelFilePath;
	private: System::Windows::Forms::TextBox^  textBoxFilePath;
	private: System::Windows::Forms::GroupBox^  groupBoxResult;
	private: System::Windows::Forms::RichTextBox^  richTextBox_Logs;

	private: System::Windows::Forms::Button^  buttonClearAll;
	private: System::Windows::Forms::Button^  buttonRun;
	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog_InputFolder;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonBrowse = (gcnew System::Windows::Forms::Button());
			this->labelFilePath = (gcnew System::Windows::Forms::Label());
			this->textBoxFilePath = (gcnew System::Windows::Forms::TextBox());
			this->groupBoxResult = (gcnew System::Windows::Forms::GroupBox());
			this->richTextBox_Logs = (gcnew System::Windows::Forms::RichTextBox());
			this->buttonClearAll = (gcnew System::Windows::Forms::Button());
			this->buttonRun = (gcnew System::Windows::Forms::Button());
			this->folderBrowserDialog_InputFolder = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->groupBoxResult->SuspendLayout();
			this->SuspendLayout();
			// 
			// buttonBrowse
			// 
			this->buttonBrowse->Location = System::Drawing::Point(490, 33);
			this->buttonBrowse->Name = L"buttonBrowse";
			this->buttonBrowse->Size = System::Drawing::Size(93, 26);
			this->buttonBrowse->TabIndex = 2;
			this->buttonBrowse->Text = L"&Browse";
			this->buttonBrowse->UseVisualStyleBackColor = true;
			this->buttonBrowse->Click += gcnew System::EventHandler(this, &GUI::btnBrowse_Click);
			// 
			// labelFilePath
			// 
			this->labelFilePath->AutoSize = true;
			this->labelFilePath->ForeColor = System::Drawing::Color::White;
			this->labelFilePath->Location = System::Drawing::Point(36, 37);
			this->labelFilePath->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelFilePath->Name = L"labelFilePath";
			this->labelFilePath->Size = System::Drawing::Size(73, 19);
			this->labelFilePath->TabIndex = 1;
			this->labelFilePath->Text = L"File Path:";
			// 
			// textBoxFilePath
			// 
			this->textBoxFilePath->Location = System::Drawing::Point(121, 33);
			this->textBoxFilePath->Margin = System::Windows::Forms::Padding(4);
			this->textBoxFilePath->Name = L"textBoxFilePath";
			this->textBoxFilePath->ReadOnly = true;
			this->textBoxFilePath->Size = System::Drawing::Size(348, 26);
			this->textBoxFilePath->TabIndex = 0;
			// 
			// groupBoxResult
			// 
			this->groupBoxResult->Controls->Add(this->richTextBox_Logs);
			this->groupBoxResult->ForeColor = System::Drawing::Color::White;
			this->groupBoxResult->Location = System::Drawing::Point(12, 66);
			this->groupBoxResult->Name = L"groupBoxResult";
			this->groupBoxResult->Size = System::Drawing::Size(608, 343);
			this->groupBoxResult->TabIndex = 4;
			this->groupBoxResult->TabStop = false;
			this->groupBoxResult->Text = L"Result :";
			// 
			// richTextBox_Logs
			// 
			this->richTextBox_Logs->Font = (gcnew System::Drawing::Font(L"Century Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->richTextBox_Logs->Location = System::Drawing::Point(6, 25);
			this->richTextBox_Logs->Name = L"richTextBox_Logs";
			this->richTextBox_Logs->ReadOnly = true;
			this->richTextBox_Logs->Size = System::Drawing::Size(596, 300);
			this->richTextBox_Logs->TabIndex = 3;
			this->richTextBox_Logs->Text = L"";
			// 
			// buttonClearAll
			// 
			this->buttonClearAll->Location = System::Drawing::Point(525, 415);
			this->buttonClearAll->Name = L"buttonClearAll";
			this->buttonClearAll->Size = System::Drawing::Size(93, 26);
			this->buttonClearAll->TabIndex = 5;
			this->buttonClearAll->Text = L"Clear &All";
			this->buttonClearAll->UseVisualStyleBackColor = true;
			this->buttonClearAll->Click += gcnew System::EventHandler(this, &GUI::buttonClearAll_Click);
			// 
			// buttonRun
			// 
			this->buttonRun->Location = System::Drawing::Point(426, 415);
			this->buttonRun->Name = L"buttonRun";
			this->buttonRun->Size = System::Drawing::Size(93, 26);
			this->buttonRun->TabIndex = 6;
			this->buttonRun->Text = L"&Run";
			this->buttonRun->UseVisualStyleBackColor = true;
			this->buttonRun->Click += gcnew System::EventHandler(this, &GUI::buttonRun_Click);
			// 
			// GUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 19);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlDarkDark;
			this->ClientSize = System::Drawing::Size(632, 451);
			this->Controls->Add(this->buttonRun);
			this->Controls->Add(this->buttonClearAll);
			this->Controls->Add(this->groupBoxResult);
			this->Controls->Add(this->buttonBrowse);
			this->Controls->Add(this->labelFilePath);
			this->Controls->Add(this->textBoxFilePath);
			this->Font = (gcnew System::Drawing::Font(L"Cambria", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Margin = System::Windows::Forms::Padding(4);
			this->Name = L"GUI";
			this->Text = L"Let The League Commence";
			this->groupBoxResult->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: bool isPathSpecified = false;		// Check if path is specified

	private: System::Void btnBrowse_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonRun_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonClearAll_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void printLogs();
	};
}
