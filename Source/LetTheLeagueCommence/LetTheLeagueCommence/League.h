// This file contains "League" class definition that are related to league

#pragma once
#include <string>
#include <vector>
#include "Season_DataStruct.h"

using namespace std;

class League
{
	private:
		int mode;											// First-fit or Best-fit (see constants.h)
		vector<Season> seasonUpdateActions;					// List of actions in a league, divided by season
	protected:
		bool RetrieveDataFromFile(string inputFile);		// Get commands from input file, restructure and store in seasonUpdateActions
		void SetMode_FirstFit();							// Set variable-length record adding mide as first-fit. 
		void SetMode_BestFit();								// Set variable-legnth-record adding mide as best-fit

		bool AddClub(string clubName);						// Add club with name <clubName> to a file (see constants.h)
		bool DeleteClub(string clubName);					// Delete club with name <clubName> to a file (see constants.h)

		bool AddPlayer(string playerName, string clubName, int season);			// Add player with name, clubName and season where the add command is applied to a file (see constant.h)
		bool DeletePlayer(string playerName, string clubName, int season);		// Delete player with name, clubName and season where the add command is applied to a file (see constant.h)
		int Helper_FindRecord(vector<string> records, int rrn);					// Helper function for variable-length record that find the record index in vector<string>records given rrn

		bool Defragment_ClubFile();									// Defragment club file (compact deleted records)
		bool Defragment_PlayerFiles(vector<string> clubInSeason);	// Defragment all player files (compact deleted records & unused space)

		bool ClonePlayerFile(vector<string> clubInSeason, int season);		// Create a clone of all current players (in a club) files in the season, where file name is specified by a list of current active club in season (clubInSeason)
	public:
		League();
		int Execute();				// Work just like main function, instead being called when neccessary
};


