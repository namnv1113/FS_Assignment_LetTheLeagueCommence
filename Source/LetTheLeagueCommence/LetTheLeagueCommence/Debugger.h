// This file contains "Debugger" class definition and related data structs which is used to store and manage logs
#pragma once
#include <string>
#include <vector>
using namespace std;

class Log
{
	public:
		int type;				// Log type (2 type: Error and Information - See constants.h)
		string information;		// Description about a log
};

class Debugger
{
	protected:
		static vector<Log> logs;		// Logs that contains execution steps, results & error information
	public:
		static void AppendLog(string description, int logTypeIdentifier);			// Add log
		static vector<Log> GetLogs();												// Get the current logs 
		static void ClearLogs();													// Clear current logs data
};

