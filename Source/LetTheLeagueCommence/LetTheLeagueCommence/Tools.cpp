// A file contains function implementations for "FileManager" class

#include "Tools.h"
#include <fstream>
#include <sstream>
#include "Debugger.h"
#include "Constants.h"
#include "FileManager.h"
#include <msclr/marshal_cppstd.h>

bool Tools::CopyFile(string source, string destination)
{
	// If source file not exist
	if (!FileManager::IsExist(source))		
	{
		Debugger::AppendLog(SOURCE_FILE_NOT_EXIST_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(SOURCE_FILE_DETAILS_STR + source, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(DESTINATION_FILE_DETAILS_STR + destination, DEBUGGER_ERROR_LOG_ID);
		return false;
	}

	// Copy file
	try
	{
		ifstream srce(source, std::ios::binary);
		ofstream dest(destination, std::ios::binary);
		dest << srce.rdbuf();
	}
	catch (exception ex)
	{
		Debugger::AppendLog(CANT_COPY_FILE_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(SOURCE_FILE_DETAILS_STR + source, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(DESTINATION_FILE_DETAILS_STR + destination, DEBUGGER_ERROR_LOG_ID);
		return false;
	}

	return true;
}

vector<string> Tools::Split(string data, char splitter)
{
	vector<string> res;
	// Create a string steam
	stringstream _strStream;
	_strStream.str(data);
	string _item;

	// If the string in stream _strStream can still be splited by splitter and store in _item
	while (getline(_strStream, _item, splitter))
	{
		// Push data to result
		res.push_back(_item);
	}

	// Return final result
	return res;
}

string Tools::IntToString(int data)
{
	stringstream ss;
	ss << data;
	return ss.str();
}

string Tools::WindowsStringToString(System::String ^ data)
{
	return msclr::interop::marshal_as< std::string >(data);
}

System::String ^ Tools::StringToWindowString(string data)
{
	return gcnew System::String(data.c_str());
}

string Tools::Join(vector<string> data, char joiner)
{
	string res = "";
	// Join strings
	for (int i = 0; i < data.size() - 1; i++)
	{
		res.append(data[i]);
		res += joiner;
	}
	// For the last character, no need to append joiner
	res.append(data[data.size() - 1]);
	return res;
}
